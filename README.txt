Alfresco User Authentication module for CMIS
--------------------------------------------

This module allows Drupal to authenticate users against an Alfresco repository, mapped via username.

Whilst logging in, an authentication ticket is stored in the user's session to enable CMIS requests
to be performed with that user's privileges.

Valid Alfresco users logging into Drupal for the first time will automatically
have a Drupal account created.


Configuration
------------- 
You must specify the url of the authentication script in the CMIS repository
config, in addition to the CMIS url. For example:

$conf['cmis_repositories'] = array(
  'default' => array(
    'url' => 'http://example.com/alfresco/s/cmis',
    'auth_url' => 'http://example.com/alfresco/s/api/login'
  )
);

If the Alfresco ticket expires, users are logged out and redirected to the login page.
You can configure Alfresco's ticket expiry behaviour in repository.properties,
e.g. via the authentication.ticket.validDuration property.
